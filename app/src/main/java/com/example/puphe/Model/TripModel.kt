package com.example.puphe.Model

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName

data class TripModel (
    var uid: String = "",
    var AcceptingDate: String = "",
    var EndingDate : String = "",
    var IDPetWalker: String = "",
    var IDDog: String = "",
    var IDUser: String = "",
    var RequestingDate: String = "",
    var StartingDate: String = "",
    var comment: String = "",
    var duration: String = "",
    var cost: String = "",
    var rating: String = "",
    var status: String = "",
    var location: LocationModel = LocationModel(),
    var petWalkerLocation: LocationModel = LocationModel()

) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "uid" to uid,
            "acceptingDate" to AcceptingDate,
            "endingDate" to EndingDate,
            "dogWalker" to IDPetWalker,
            "dogs" to IDDog,
            "user" to IDUser,
            "requestingDate" to RequestingDate,
            "startingDate" to StartingDate,
            "comment" to comment,
            "duration" to duration,
            "cost" to cost,
            "raiting" to rating,
            "status" to status,
            "location" to location,
            "petWalkerLocation" to petWalkerLocation
        )
    }
}