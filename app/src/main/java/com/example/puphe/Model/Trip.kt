package com.example.puphe.Model

import com.google.firebase.database.Exclude

class Trip (
    var uid: String,
    var status: String,
    var requestingDate: String,
    var acceptingDate: String,
    var startingDate: String,
    var endingDate: String,
    var duration: String,
    var rating: String,
    var cost: Double,
    var comment: String,
    var dogWalker: String,
    var user: String,
    var dogs: ArrayList<String>,
    var location: HashMap<String, Double>,
    var petWalkerLocation: HashMap<String, Double>,
    var meters: String
){
    @Exclude
    fun toMap(): Map<String, Any?>{
        return hashMapOf(
                "status" to status,
                "requestingDate" to requestingDate,
                "acceptingDate" to acceptingDate,
                "startingDate" to startingDate,
                "endingDate" to endingDate,
                "duration" to duration,
                "rating" to rating,
                "cost" to cost,
                "comment" to comment,
                "dogWalker" to dogWalker,
                "user" to user,
                "dogs" to dogs,
                "location" to location,
                "petWalkerLocation" to petWalkerLocation,
                "meters" to meters
        )
    }

    constructor(): this(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            0.0,
            "",
            "",
            "",
            ArrayList(),
            hashMapOf<String,Double>(
                    "longitude" to 0.0,
                    "latitude" to 0.0
            ),
            hashMapOf<String,Double>(
                    "longitude" to 0.0,
                    "latitude" to 0.0
            ),
            ""
    ){

    }
}