package com.example.puphe.Model

import com.google.firebase.firestore.PropertyName

class LocationModel (
    @PropertyName("latitude") val latitude: String = "",
    @PropertyName("longitude") val longitude: String = ""
)