package com.example.puphe.Model

data class WalkerInfoModel (
     var name:String="",
     var email:String="",
     var lastName:String="",
     var birthday:String="",
     var Url_Image:String="",
     var rating:Double=0.0,
     var meters:String="0.0",
     var bio:String=""
)