package com.example.puphe

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class DogActivity : AppCompatActivity(){

    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dog_main)

        //setup
        val bundle = intent.extras
        val email = bundle?.getString("email")
        config(email?:"")
    }

    private fun config(email: String){
        title = "Your dog's profile"
        val editTextDogName = findViewById<EditText>(R.id.editTextDogName)
        val editTextBirthYear = findViewById<EditText>(R.id.editTextBirthYear)
        val editTextDogBreed = findViewById<EditText>(R.id.editTextDogBreed)
        val buttonSaveDog = findViewById<Button>(R.id.buttonSaveDog)
        val seeDogsButton = findViewById<Button>(R.id.seeDogsButton)
        buttonSaveDog.setOnClickListener {
            db.collection("dog").add(
                    hashMapOf("name" to editTextDogName.text.toString(),
                            "birthday_year" to editTextBirthYear.text.toString(),
                            "breed" to editTextDogBreed.text.toString(),
                            "email_owner" to email
                    )
            )
        }
        seeDogsButton.setOnClickListener {
            showUserDogs(email)
        }
    }

    private fun showUserDogs(email: String){
        val walkRequestIntent = Intent(this, WalkRequestActivity::class.java).apply{
            putExtra("email", email)
        }
        startActivity(walkRequestIntent)
    }

}