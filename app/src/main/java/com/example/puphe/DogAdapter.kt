package com.example.puphe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class DogAdapter (val mCtx: Context, val layoutResId: Int, val dogList: List<Dog>)
    :ArrayAdapter<Dog>(mCtx, layoutResId, dogList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null)
        val textViewName = view.findViewById<TextView>(R.id.textViewDogName)
        val dog = dogList[position]
        textViewName.text = dog.name
        return view
    }
}