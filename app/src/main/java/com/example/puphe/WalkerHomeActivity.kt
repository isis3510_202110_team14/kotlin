package com.example.puphe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.puphe.Model.Trip
import com.example.puphe.ui.home.HomeFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.profile_edit_activity.*
import kotlinx.coroutines.*
import java.lang.Exception
import java.lang.StringBuilder
import java.time.LocalDateTime

class WalkerHomeActivity : AppCompatActivity() {

    /**
     * Constantes
     */
    companion object {
        /* Constante que define el estado de un viaje en espera a confirmación*/
        val WAITING = "Waiting"

        /* Constante que define el estado de un viaje que fue aceptado y esta a la espera de recogida */
        val ACCEPTED = "Accepted"

        /* Constante que define el estado de un viaje que está actualmente siendo realizado*/
        val ON_GOING = "Ongoing"

        /* Constante que defineel estado de un viaje cuando el paseador está regresando a devolver el perro */
        val RETURNING = "Returning"

        /* Constante que define el estado de un viaje que está actualmente finalizado */
        val FINISHED = "Finished"

        val COLLECTION_TRIP_NAME = "trip"

        val COLLECTION_FIRESTORE_DOG_WALKERS_NAME = "dog_walker"

        val MAXIMUM_DISTANCE = 100000000
    }

    // World
    private lateinit var currentTrip: Trip
    private lateinit var currentDog: HashMap<String, Any?>

    // Memoria
    private var availableTrips: MutableList<Trip> = mutableListOf()
    private var refusedIdsTrips: MutableList<String> = mutableListOf()

    // Views
    private lateinit var layout_welcome: LinearLayout
    private lateinit var layout_buttons: LinearLayout
    private lateinit var layout_accept:CardView
    private lateinit var layout_request:LinearLayout
    private lateinit var layout_arrived:LinearLayout
    private lateinit var layout_trip_in_progress:LinearLayout
    private lateinit var txt_time:TextView

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navView:NavigationView
    private lateinit var drawerLayout:DrawerLayout
    private lateinit var navController:NavController
    private lateinit var img_avatar:ImageView
    private lateinit var trips: List<String>
    private var index:Int=0

    private var currentLat:Double=0.0
    private var currentLng:Double=0.0
    private var currentMeters = MutableLiveData<String>()
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val defaultScope = CoroutineScope(Job() + Dispatchers.Default)

    // Buttons
    private lateinit var btn_accept_trip: Button
    private lateinit var btn_reject_trip: Button
    private lateinit var btn_arrived_trip: Button
    private lateinit var btn_comming_back_trip: Button
    private lateinit var btn_finished_trip: Button

    // Fragments
    private lateinit var fragment : HomeFragment

    // State
    enum class SystemStatus {
        WAITING, ON_TRIP
    }

    private var systemStatus : SystemStatus = SystemStatus.WAITING
    private var followUbicationCamera: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walker_home)

        //Toolbar
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //Toolbar

        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        navView = findViewById(R.id.nav_view)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        init()
        addListener()
        createMeterCounterTotal()
        getTrips()
        location()

        GlobalScope.launch{
            try {
                while (true) {
                    location()
                    delay(10000L)
                    for (i in trips) {
                        updateLocation(i, currentLng, currentLat)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun persistingResults(){
        val db = Firebase.firestore
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE)
        val data = hashMapOf("meters" to sharedPref.all.get(getString(R.string.totalMeters)).toString())
        db.collection(COLLECTION_FIRESTORE_DOG_WALKERS_NAME).document(FirebaseAuth.getInstance().currentUser!!.email)
                .set(data, SetOptions.merge())
    }

    fun location() {

        locationRequest = LocationRequest()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setFastestInterval(3000)
        locationRequest.interval = 5000
        locationRequest.setSmallestDisplacement(10f)

        locationCallback = object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                val newPos = LatLng(locationResult!!.lastLocation.latitude, locationResult!!.lastLocation.longitude)
                currentLat=locationResult.lastLocation.latitude
                currentLng= locationResult.lastLocation.longitude

                if(followUbicationCamera){
                    fragment.animateCamaraClose(LatLng(currentLat, currentLng), 20f)
                }
            }
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this!!)
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
    }

    private fun createMeterCounterTotal() {
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE)

        if(sharedPref.all.get(getString(R.string.totalMeters))==null){

            // Vuelve a obtener el viaje para verificar que no haya sido tomado por otro usuario
            val db = Firebase.firestore
            db.collection(COLLECTION_FIRESTORE_DOG_WALKERS_NAME).document(FirebaseAuth.getInstance().currentUser.email).get().addOnSuccessListener{

                var metersToSave: Double = 0.0

                if(it.contains("meters")){
                    var metersToSaveStr = it.get("meters") as String
                    metersToSave = metersToSaveStr.toDouble()
                }

                with(sharedPref.edit()){
                    putFloat(getString(R.string.totalMeters), metersToSave.toFloat())
                    commit()
                }
            }
        }
    }


    private fun createMeterCounterCurrentTrip() {
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putFloat(getString(R.string.currentMeters), 0.0F)
            commit()
        }
    }

    private fun saveMetersCount(newMeters: Float){
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE)
        var currentTotalMeters = sharedPref.all.get(getString(R.string.totalMeters)) as Float
        var currentOnTripMeters = sharedPref.all.get(getString(R.string.currentMeters)) as Float

        with(sharedPref.edit()) {
            putFloat(getString(R.string.totalMeters), currentTotalMeters + newMeters)
            putFloat(getString(R.string.currentMeters), currentOnTripMeters + newMeters)
            commit()
        }

    }

    override fun onStart() {
        super.onStart()
        // Layouts
        layout_accept = findViewById(R.id.layout_accept) as CardView
        layout_buttons = findViewById(R.id.layout_buttons) as LinearLayout
        layout_request = findViewById(R.id.layout_request) as LinearLayout
        layout_arrived = findViewById(R.id.layout_on_the_way) as LinearLayout
        layout_welcome = findViewById(R.id.layout_welcome) as LinearLayout
        layout_trip_in_progress = findViewById(R.id.layout_trip_in_progress) as LinearLayout

        // Buttons
        btn_accept_trip = layout_buttons.findViewById(R.id.btn_accept_trip) as Button
        btn_reject_trip = layout_buttons.findViewById(R.id.btn_reject_trip) as Button
        btn_arrived_trip = findViewById(R.id.btn_arrived_trip) as Button
        btn_finished_trip = findViewById(R.id.btn_finish_trip) as Button
        btn_comming_back_trip = findViewById(R.id.btn_coming_back_trip)

        btn_accept_trip.setOnClickListener {
            takeTrip(availableTrips.last())
        }

        btn_reject_trip.setOnClickListener {
            rejectTrip(availableTrips.last().uid)
        }

        btn_arrived_trip.setOnClickListener{
            startTrip()
        }

        btn_finished_trip.setOnClickListener{
            endTrip()
        }

        btn_comming_back_trip.setOnClickListener {
            returnigTrip()
        }

        /* Observa el cambio de los metros*/
        currentMeters.observe(this, Observer {
            txt_estimate_walks.text = it + " meters "
        })

        /* Inicializa el txt del tiempo */
        txt_time = findViewById(R.id.txt_time_elapsed)


    }

    private fun parseDataSnapshotToTripModel(snapshot: DataSnapshot): MutableList<Trip>{
        val list: MutableList<Trip> = mutableListOf()
        val children = snapshot!!.children
        children.forEach{
            try {
                var tripJson = it.getValue(Trip::class.java)!!
                tripJson.uid = it.key.toString()
                list.add(tripJson)
            } catch(e: Exception) {
                e.printStackTrace()
                Log.e("ERROR", "It was not possible to convert a trip from database")
            }
        }
        return list
    }

    private fun updateTrip(updatedTrip: Trip) {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val reference = firebaseDatabase.getReference()

        val uid = updatedTrip.uid
        val childUpdates = hashMapOf<String, Any>(
            "/$COLLECTION_TRIP_NAME/$uid" to updatedTrip
        )

        reference.updateChildren(childUpdates)
    }

    private fun rejectTrip(tripId: String) {
        refusedIdsTrips.add(tripId)
        showRequestTripViews(false)
        fragment.clearMap()
        showWelcome(true)
    }

    private fun takeTrip(trip:Trip){
        if(!availableTrips.isEmpty() && !refusedIdsTrips.contains(trip.uid)) {
            currentTrip = trip
            val firebaseDatabase = FirebaseDatabase.getInstance()
            val reference = firebaseDatabase.getReference()

            createMeterCounterCurrentTrip()

            // Vuelve a obtener el viaje para verificar que no haya sido tomado por otro usuario
            reference.child(COLLECTION_TRIP_NAME).child(trip.uid).get().addOnSuccessListener {
                val realtimeTrip: Trip? = it.getValue(Trip::class.java)
                if (realtimeTrip!!.status == WAITING) {
                    // Cambia el estado y la fecha de aceptación
                    currentTrip.acceptingDate = LocalDateTime.now().toString()

                    val user = FirebaseAuth.getInstance().currentUser
                    currentTrip.dogWalker = user.email
                    currentTrip.status = ACCEPTED
                    currentTrip.petWalkerLocation = hashMapOf<String,Double>(
                        "longitude" to currentLng,
                        "latitude" to currentLat
                    )

                    //Realiza la actualización en la base de datos
                    updateTrip(currentTrip)

                    // Cambia el estado del sistema y oculta el menu del viaje
                    systemStatus = SystemStatus.ON_TRIP
                    showRequestTripViews(false)

                    // Pasa a la siguiente vista
                    showOnTheWay(true)

                    //Muestra la info en la pestalla on the way.
                    val txt_breed = findViewById(R.id.dog_name) as TextView
                    val txt_name = findViewById(R.id.dog_breed) as TextView
                    val txt_breed_ontrip = findViewById(R.id.dog_name_ontrip) as TextView
                    val txt_name_ontrip = findViewById(R.id.dog_breed_ontrip) as TextView

                    txt_breed.text = currentDog.get("name").toString()
                    txt_name.text = currentDog.get("breed").toString()
                    txt_breed_ontrip.text = currentDog.get("name").toString()
                    txt_name_ontrip.text = currentDog.get("breed").toString()

                    //Send information of geolocalization
                    getTrips()
                    fragment.animateCamaraClose(LatLng(currentLat, currentLng), 20f)
                    followUbicationCamera = true
                }
            }
        }

    }

    private fun startTrip(){
        /* Ejecuta el servicio de  */
        currentTrip.startingDate = LocalDateTime.now().toString()
        currentTrip.status = ON_GOING
        updateTrip(currentTrip)

        showOnTheWay(false)
        showTripInProgress(true)
        currentMeters.value = "0"

    }

    private fun returnigTrip(){
        currentTrip.status = RETURNING
        updateTrip(currentTrip)

        btn_comming_back_trip.visibility = View.GONE
        btn_finished_trip.visibility = View.VISIBLE
    }

    private fun endTrip(){
        currentTrip.endingDate = LocalDateTime.now().toString()
        currentTrip.status = FINISHED
        updateTrip(currentTrip)

        showTripInProgress(false)
        persistingResults()
        systemStatus = SystemStatus.WAITING
        getTrips()

        followUbicationCamera = false
        fragment.animateCamaraClose(LatLng(currentLat, currentLng), 17f)
        fragment.clearMap()
        showWelcome(true)
    }

    private fun addListener() {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val reference = firebaseDatabase.getReference()
        reference.child(COLLECTION_TRIP_NAME).addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("Cancelled", "Close operation in database: ${error.message}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (systemStatus == SystemStatus.WAITING) {
                    availableTrips = parseDataSnapshotToTripModel(snapshot);
                    availableTrips = selectBestTrips(availableTrips)

                    if (availableTrips.isEmpty()) {
                        showRequestTripViews(false)
                    }
                    else {
                        showWelcome(false)
                        showRequestTripViews(true)
                        // Agrega la información del perro
                        val db = Firebase.firestore
                        val idDog = availableTrips.last().dogs.get(0)
                        val docRef = db.collection("user").document(availableTrips.last().user).collection("dogs").document(idDog)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    //val dogImage = document.get("image")
                                    currentDog = document.data as HashMap<String, Any?>
                                    val dogImage = "https://firebasestorage.googleapis.com/v0/b/puphe-8dc6d.appspot.com/o/assets%2Fdefault%2Fdog.jpg?alt=media&token=8160a8c0-c002-45ed-9ff5-e9261ccec22e"
                                    val img_dog_accepting = findViewById(R.id.img_dog_accept) as ImageView
                                    val img_dog_ongoing = findViewById(R.id.img_dog_ongoing) as ImageView
                                    val img_dog_ontrip = findViewById(R.id.img_dog_ontrip) as ImageView

                                    Glide.with(this@WalkerHomeActivity).load(dogImage).into(img_dog_accepting)
                                    Glide.with(this@WalkerHomeActivity).load(dogImage).into(img_dog_ongoing)
                                    Glide.with(this@WalkerHomeActivity).load(dogImage).into(img_dog_ontrip)

                                } else {
                                    Log.d("ERROR", "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d("ERROR", "get failed with ", exception)
                            }


                        // Muestra ambas ubicaciones
                        val destiny = availableTrips.last().location
                        val currentOrigin = LatLng(currentLat, currentLng)
                        val currentDestiny = LatLng(destiny.get("latitude")!!.toDouble(), destiny.get("longitude")!!.toDouble())

                        val txt_estimate_time =  findViewById(R.id.txt_estimate_time) as TextView
                        val txt_estimate_distance =  findViewById(R.id.txt_estimate_distance) as TextView

                        val txt_estimate_time_on_going =  findViewById(R.id.txt_estimate_time_ongoing) as TextView
                        val txt_estimate_distance_on_going =  findViewById(R.id.txt_estimate_distance_ongoing) as TextView

                        fragment = supportFragmentManager.fragments[0].childFragmentManager.fragments[0] as HomeFragment
                        fragment!!.traceRoute(currentOrigin, currentDestiny,
                            txt_estimate_time, txt_estimate_distance, txt_estimate_time_on_going, txt_estimate_distance_on_going)
                    }
                }
            }

        })
    }

    private fun selectBestTrips(availableTrips: MutableList<Trip>): MutableList<Trip> {
        availableTrips.retainAll { it.status == WAITING}
        val bestTrips = mutableListOf<Trip>()
        availableTrips.forEach{trip ->
            var distaceInMeters = distance(currentLat, currentLng, trip.location!!.get("latitude") as Double, trip.location!!.get("longitude") as Double )
            if(distaceInMeters <= MAXIMUM_DISTANCE)
                bestTrips.add(trip)
        }
        return bestTrips
    }

    private fun showRequestTripViews(show: Boolean) {
        if(show)
            layout_request.visibility = View.VISIBLE
        else
            layout_request.visibility = View.GONE
    }

    private fun getTrips(){
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val reference = firebaseDatabase.getReference(COLLECTION_TRIP_NAME)
        trips = mutableListOf<String>()
        reference.get().addOnSuccessListener {
            it.children.forEach{trip ->
                //TODO: Modificar esto para que no dependa de un string
                val email = trip.child("dogWalker").value
                val email2 = FirebaseAuth.getInstance().currentUser!!.email
                if(trip.child("dogWalker").value==FirebaseAuth.getInstance().currentUser!!.email &&
                    (trip.child("status").value== ACCEPTED ||
                        trip.child("status").value== ON_GOING)){
                    (trips as MutableList<String>).add(trip.key.toString())
                }
            }
        }.addOnFailureListener {
            Log.e("Error", "Not possible to reach trips")
        }
    }

    suspend fun updateLocation(id:String, longitude:Double, latitude:Double){
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val reference = firebaseDatabase.getReference(COLLECTION_TRIP_NAME)
        var ans: Trip
        withContext(Dispatchers.IO) {
            reference.child(id).get().addOnSuccessListener {
                ans = it.getValue(Trip::class.java)!!
                val lonAnt = ans!!.petWalkerLocation.get("longitude") as Double
                val latAnt = ans!!.petWalkerLocation.get("latitude") as Double
                if(latAnt != 0.0 && lonAnt != 0.0 && longitude != 0.0 && latitude != 0.0){
                    var d = distance(latAnt, lonAnt, latitude, longitude)

                    // Pone los pasos totales y parciales
                    saveMetersCount(d.toFloat())

                    if(ans!!.meters==""){
                        ans!!.meters = "0.0"
                    }else{
                        ans!!.meters = (ans.meters.toDouble() + d).toString()
                        currentTrip.meters = ans!!.meters
                        currentMeters.postValue(String.format("%.1f", currentTrip.meters.toDouble()))
                    }

                }
                ans!!.petWalkerLocation = hashMapOf<String,Double>(
                    "longitude" to longitude,
                    "latitude" to latitude
                )

                val tripValues = ans!!.toMap()
                val childUpdates = hashMapOf<String, Any>(
                    id to tripValues
                )
                reference.updateChildren(childUpdates)
            }.addOnFailureListener {
                Log.e("Error", "Hola")
            }
        }
    }

    private fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double{
        val r =6371000
        val phi1=lat1*Math.PI/180
        val phi2=lat2*Math.PI/180
        val deltaPhi=(lat2-lat1)*Math.PI/180
        val deltaLambda=(lon2-lon1)*Math.PI/180
        val a=Math.sin(deltaPhi/2)*Math.sin(deltaPhi/2)+
                Math.cos(phi1)*Math.cos(phi2)*Math.sin(deltaLambda/2)*Math.sin(deltaLambda/2)
        val c=Math.atan2(Math.sqrt(a),Math.sqrt(1-a))
        return r*c
    }

    private fun  showOnTheWay(show: Boolean){
        if(show)
            layout_arrived.visibility = View.VISIBLE
        else
            layout_arrived.visibility = View.GONE
    }

    private fun  showWelcome(show: Boolean){
        if(show)
            layout_welcome.visibility = View.VISIBLE
        else
            layout_welcome.visibility = View.GONE
    }

    private fun showTripInProgress(show: Boolean){
        if(show)
            layout_trip_in_progress.visibility = View.VISIBLE
        else
            layout_trip_in_progress.visibility = View.GONE
    }

    private fun init() {
        navView.setNavigationItemSelectedListener {
            if(it.itemId == R.id.nav_sign_out){
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Sign out")
                    .setMessage("Do you really want to sign out?")
                    .setNegativeButton("CANCEL") { dialogInterface, _ -> dialogInterface.dismiss()  }
                    .setPositiveButton("SIGN OUT") { dialogInterface, _ ->
                        FirebaseAuth.getInstance().signOut()
                        val intent = Intent(this, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        finish()
                    }.setCancelable(false )

                val dialog = builder.create()
                dialog.setOnShowListener {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(resources.getColor(android.R.color.holo_red_dark))
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                        .setTextColor(resources.getColor(R.color.colorAccent))
                }

                dialog.show()
            }
            if(it.itemId == R.id.nav_profile){
                startActivity(Intent(this,ProfileViewActivity::class.java))
                Picasso.get().load(Common.currentUser!!.Url_Image).into(img_avatar)
            }
            if(it.itemId == R.id.refresh){
                actualizarRaiting()
                Picasso.get().load(Common.currentUser!!.Url_Image).into(img_avatar)
            }

            true
        }

        val headerView = navView.getHeaderView(0)
        val txt_name = headerView.findViewById<View>(R.id.txt_name) as TextView
        val txt_phone = headerView.findViewById<View>(R.id.txt_phone) as TextView
        val txt_star = headerView.findViewById<View>(R.id.txt_star) as TextView
        img_avatar = headerView.findViewById<View>(R.id.img_avatar) as ImageView

        txt_name.setText(Common.buildWelcomeMessage())
        txt_star.setText(StringBuilder().append(Common.currentUser!!.rating))
        Picasso.get().load(Common.currentUser!!.Url_Image).into(img_avatar)

    }

    private fun actualizarRaiting() {
        val db = FirebaseFirestore.getInstance()
        var totalCOst=0.0
        var f=1.0
        val docRef = db.collection(COLLECTION_FIRESTORE_DOG_WALKERS_NAME).document(FirebaseAuth.getInstance().currentUser!!.email)
        db.collection(COLLECTION_TRIP_NAME)
            .whereEqualTo("dogWalker", docRef)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("ProfileVIewActivity:", "${document.id} => ${document.data}")
                    val cost = document.get("rating").toString()
                    totalCOst = totalCOst + cost.toDouble()
                    f = f+1


                }
                if(f.equals(1.0)){
                    Common.currentUser!!.rating = 0.0
                    docRef.update("rating",Common.currentUser!!.rating)
                    txt_star.text=Common.currentUser!!.rating.toString()
                }
                else{
                    Common.currentUser!!.rating = totalCOst/(f-1)
                    docRef.update("rating",Common.currentUser!!.rating)
                    txt_star.text=Common.currentUser!!.rating.toString()
                }


            }
            .addOnFailureListener { exception ->
                Log.w("ProfileVIewActivity:", "Error getting documents: ", exception)
            }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.driver_home, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

//    fun updateMinutes(delay: Long) {
//        val handler = Handler()
//        handler.postDelayed({
//            this@WalkerHomeActivity.runOnUiThread {
//                var r = {
//                        if(systemStatus == SystemStatus.ON_TRIP) {
//                            txt_time.text = (txt_time.text.toString().toInt() + 1).toString()
//                            handler.postDelayed(this, delay)
//                }
//
//                Runnable {
//                    r()
//                }
//            }
//            }
//        }, delay)
//    }

}