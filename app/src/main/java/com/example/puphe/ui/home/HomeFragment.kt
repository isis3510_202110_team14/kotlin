package com.example.puphe.ui.home

import android.content.res.Resources
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.puphe.Common
import com.example.puphe.R
import com.firebase.geofire.GeoFire
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.maps.route.extensions.drawMarker
import com.maps.route.extensions.drawRouteOnMap
import com.maps.route.extensions.moveCameraOnMap

class HomeFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var mapFragment:SupportMapFragment

    // location
    private lateinit var locationRequest:LocationRequest
    private lateinit var locationCallback:LocationCallback
    private lateinit var fusedLocationProviderClient:FusedLocationProviderClient

    //Online system
    private lateinit var onlineRef:DatabaseReference
    private lateinit var currentUserRef:DatabaseReference
    private lateinit var driversLocationRef:DatabaseReference
    private lateinit var geoFire:GeoFire

    private val onlineValueEventListener = object:ValueEventListener{
        override fun onCancelled(error: DatabaseError) {
            Snackbar.make(mapFragment.requireView(), error.message, Snackbar.LENGTH_LONG).show()
        }

        override fun onDataChange(snapshot: DataSnapshot) {
            if(snapshot.exists())
                currentUserRef.onDisconnect().removeValue()
        }

    }


    override fun onDestroy() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        onlineRef.removeEventListener(onlineValueEventListener)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        registerOnlineSystem()
    }

    private fun registerOnlineSystem() {
        onlineRef.addValueEventListener(onlineValueEventListener)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        init()



        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    private fun init() {

        onlineRef = FirebaseDatabase.getInstance().getReference().child(".info/connected")
        driversLocationRef = FirebaseDatabase.getInstance().getReference(Common.WALKER_LOCATION_REFERENCE)
        currentUserRef = FirebaseDatabase.getInstance().getReference(Common.WALKER_LOCATION_REFERENCE).child(
            FirebaseAuth.getInstance().currentUser!!.uid
        )

        geoFire = GeoFire(driversLocationRef)

        registerOnlineSystem()


        locationRequest = LocationRequest()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setFastestInterval(3000)
        locationRequest.interval = 5000
        locationRequest.setSmallestDisplacement(10f)

        locationCallback = object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                val newPos = LatLng(locationResult!!.lastLocation.latitude, locationResult!!.lastLocation.longitude)

                val yourLocation: CameraUpdate = CameraUpdateFactory.newLatLngZoom(newPos, 17f)
                mMap.animateCamera(yourLocation);



//                //Update Location
//                geoFire.setLocation(
//                    FirebaseAuth.getInstance().currentUser!!.uid,
//                    GeoLocation(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
//                ) { key:String?, error:DatabaseError? ->
//                    if(error != null)
//                        Snackbar.make(mapFragment.requireView(), error.message, Snackbar.LENGTH_LONG).show()
//                    else
//                        Snackbar.make(mapFragment.requireView(), "You're Online!", Snackbar.LENGTH_LONG).show()
//
//
//                }

            }


        }



        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext()!!)
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    fun animateCamaraClose(yourLocation: LatLng, zoom: Float){
        val yourLocation: CameraUpdate = CameraUpdateFactory.newLatLngZoom(yourLocation, zoom)
        mMap.animateCamera(yourLocation);
    }

    fun clearMap(){
        mMap.clear()
    }


    fun traceRoute(origin: LatLng, destiny: LatLng, txt_estimate_time: TextView,
                   txt_estimate_distance: TextView,
                   txt_estimate_time_ongoing: TextView,
                   txt_estimate_distance_ongoing: TextView) {

        mMap?.run {
            //if you want to move the map on specific location
            moveCameraOnMap(latLng = destiny, zoom = 17f)

            //if you want to drop a marker of maps, call it
            drawMarker(location = origin, context = requireContext(), title = "test marker")

            drawRouteOnMap(
                mapsApiKey = getString(R.string.google_maps_key),  // YOur API key
                context = requireContext(), //App context
                source = origin, //Source, from where you want to draw path
                destination = destiny  //Destination, to where you want to draw path
            ) { estimates ->
                estimates?.let {
                    //Google Estimated time of arrival
                    txt_estimate_time.text = it.duration?.text
                    txt_estimate_time_ongoing.text = it.duration?.text

                    //Google suggested path distance
                    txt_estimate_distance.text = it.distance?.text
                    txt_estimate_distance_ongoing.text = it.distance?.text

                } ?: Log.e("ERROR", "Nothing found")
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        //Request permission
        Dexter.withContext(requireContext()!!)
            .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object:PermissionListener{
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    // Enable button first
                    mMap.isMyLocationEnabled = true
                    mMap.uiSettings.isMyLocationButtonEnabled = true
                    mMap.setOnMyLocationClickListener {
                        fusedLocationProviderClient.lastLocation
                            .addOnFailureListener{e ->
                                Toast.makeText(context!!, e.message, Toast.LENGTH_SHORT).show()
                            }.addOnSuccessListener { location ->
                                val userLatLng = LatLng(location.latitude, location.longitude)
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 18f))
                            }
                    }

                    //Layout
                    val view = mapFragment.requireView()!!
                        .findViewById<View>("1".toInt())!!
                            .parent!! as View
                    val locationButton = view.findViewById<View>("2".toInt())
                    val params = locationButton.layoutParams as RelativeLayout.LayoutParams
                    params.addRule(RelativeLayout.ALIGN_TOP, 0)
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                    params.bottomMargin = 50
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    TODO("Not yet implemented")
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    Toast.makeText(context!!, "Permission " + p0!!.permissionName+ " was denied", Toast.LENGTH_SHORT).show()
                }

            }).check()


        try{
            val success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.uber_maps_style))
            if(!success)
                Log.e("EDMT_ERROR", "Style parsing error")
        } catch (e:Resources.NotFoundException){
            Log.e("EDTM_ERROR", e.message.toString())
        }
    }
}