package com.example.puphe.ui.utils

import android.content.Context
import android.content.Intent
import android.widget.Toast


fun Context.toast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


