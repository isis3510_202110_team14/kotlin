package com.example.puphe

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.example.puphe.Model.WalkerInfoModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore

class  MainActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val dbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Puphe_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_main)

        //Analytics feature event
        val analytics:FirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString("message","Integración Firebase satisfactoria")
        analytics.logEvent("Initialization", bundle)

        //setup
        config()

        /// Create liveData of Eventual Connectivity
        val networkConnection = NetworkConnection(applicationContext)
        networkConnection.observeForever( Observer {
            isConnected ->
                if(!isConnected) {
                    val intent = Intent(applicationContext, CheckInternetConnectionActivity::class.java)
                    startActivity(intent)
                }
        })

//        val crashButton = Button(this)
//        crashButton.text = "Crash!"
//        crashButton.setOnClickListener {
//            throw RuntimeException("Test Crash") // Force a crash
//        }
//
//        addContentView(crashButton, ViewGroup.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT))
    }

    private fun config(){

        // SETUP
        title = "Authentication"
        val signUpButton = findViewById<Button>(R.id.signUpButton)
        val logInButton = findViewById<Button>(R.id.signOutButton)
        val emailAuthEditText = findViewById<EditText>(R.id.emailAuthEditText)
        val passwordAuthEditText = findViewById<EditText>(R.id.passwordAuthEditText)

        var email = ""
        var password = ""

        signUpButton.setOnClickListener{
            signUpButton.setEnabled(false)
            logInButton.setEnabled(false)

            email = emailAuthEditText.text.toString()
            password = passwordAuthEditText.text.toString()

            if(emailAuthEditText.text.isNotEmpty() && passwordAuthEditText.text.isNotEmpty()){
                dbAuth.createUserWithEmailAndPassword(email, password).
                addOnCompleteListener{
                    if(it.isSuccessful){
                        showHome(it.result?.user?.email?:"")
                        signUpButton.setEnabled(true)
                        logInButton.setEnabled(true)
                    }
                    else{
                        signUpButton.setEnabled(true)
                        logInButton.setEnabled(true)
                        showAlert()
                    }
                }
            }
            else{
                signUpButton.setEnabled(true)
                logInButton.setEnabled(true)
            }
        }

        logInButton.setOnClickListener{
            logInButton.setEnabled(false)
            signUpButton.setEnabled(false)
            email = emailAuthEditText.text.toString()
            password = passwordAuthEditText.text.toString()

            if(emailAuthEditText.text.isNotEmpty() && passwordAuthEditText.text.isNotEmpty()){

                    dbAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener{
                        if(it.isSuccessful){
                            db.collection("dog_walker").document(email).get().addOnSuccessListener {document ->
                                if(document.exists()){
                                    val model:WalkerInfoModel? = document.toObject(WalkerInfoModel::class.java)
                                    logInButton.setEnabled(true)
                                    signUpButton.setEnabled(true)
                                    goToDriverActivity(model)
                                }

                            }
                        }
                        else{
                            logInButton.setEnabled(true)
                            signUpButton.setEnabled(true)
                            showAlert()
                        }
                    }
            }
            logInButton.setEnabled(true)
            signUpButton.setEnabled(true)
        }
    }

    private fun goToDriverActivity(model:WalkerInfoModel?) {
        Common.currentUser = model
        startActivity(Intent(this,WalkerHomeActivity::class.java))
        finish()
    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("There was a problem with the credentials")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showHome(email: String){
        val homeIntent = Intent(this, HomeActivity::class.java).apply{
            putExtra("email", email)
        }
        startActivity(homeIntent)
    }

}