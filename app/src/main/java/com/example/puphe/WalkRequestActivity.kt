package com.example.puphe

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class WalkRequestActivity : AppCompatActivity(){

    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.walk_request_main)

        //setup
        val bundle = intent.extras
        val email = bundle?.getString("email")
        config(email?:"")
    }

    private fun config(email: String){
        title = "Your Dogs"
        val listViewWalkRequest = findViewById<ListView>(R.id.listViewWalkRequest)
        val dogList = mutableListOf<Dog>()
        db.collection("dog").whereEqualTo("email_owner", email).get().addOnCompleteListener { task ->
            if( task.isSuccessful ){
                val document = task.result
                Log.d("List", document?.documents.toString())
                if (document != null) {
                    for(item in document.documents){
                        val dog = Dog(id=item["id"].toString(),
                                birthday_year=item["birthday_year"].toString(),
                                breed=item["breed"].toString(),
                                email_owner=item["email_owner"].toString(),
                                name=item["name"].toString())
                        dogList.add(dog)
                        Log.d("List", item["id"].toString())
                    }
                    val adapter = DogAdapter(applicationContext, R.layout.dog, dogList)
                    listViewWalkRequest.adapter = adapter
                }
            }
            else {
            }
        }

    }
}