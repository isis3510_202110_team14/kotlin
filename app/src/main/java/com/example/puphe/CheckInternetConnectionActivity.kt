package com.example.puphe

import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer

class CheckInternetConnectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_internet_connection)

        val networkConnection = NetworkConnection(applicationContext)
        networkConnection.observe( this, Observer {
                isConnected ->
            if(isConnected) {
                finish()
            }
        })
    }

    override fun onBackPressed() {
        // it doesnt do nothing.
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}