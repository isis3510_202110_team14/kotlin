package com.example.puphe

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.puphe.Model.WalkerInfoModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.home_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.*

class  HomeActivity : AppCompatActivity(){

    companion object {
        val TAG = "RegisterActivity"
    }

    private val db = FirebaseFirestore.getInstance()
    private val db2 = FirebaseAuth.getInstance()
    lateinit var database:FirebaseDatabase
    lateinit var walkerInfoRef: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_main)


        //setup
        val bundle = intent.extras
        val email = bundle?.getString("email")
        config(email?:"")

        selectphoto_button_register.setOnClickListener {
            Log.d(TAG, "Try to show photo selector")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    var selectedPhotoUri: Uri? = null
    lateinit var selectedPhotoString: String

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            // proceed and check what the selected image was....
            Log.d(TAG, "Photo was selected")

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            selectphoto_imageview_register.setImageBitmap(bitmap)
            selectphoto_button_register.alpha = 0f

//      val bitmapDrawable = BitmapDrawable(bitmap)
//      selectphoto_button_register.setBackgroundDrawable(bitmapDrawable)
        }
    }

    private fun config(email: String){

        database = FirebaseDatabase.getInstance()
        walkerInfoRef = database.getReference(Common.WALKER_INFO_REFERENCE)

        lateinit var database:FirebaseDatabase
        lateinit var walkerInfoRef:DatabaseReference

        title = "Home"
        val emailView = findViewById<TextView>(R.id.emailView)
        val editTextName = findViewById<EditText>(R.id.editTextName)
        val editTextLastName = findViewById<EditText>(R.id.editTextLastName)
        val mPickTimeBtn = findViewById<Button>(R.id.pickDateBtn)
        val textView     = findViewById<TextView>(R.id.dateTv)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        mPickTimeBtn.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                textView.setText("" + (dayOfMonth) + "/" + (monthOfYear+1) + "/" + year)
            }, year, month, day)
            dpd.show()}
        val saveButton = findViewById<Button>(R.id.saveButton)
        val signOutButton = findViewById<Button>(R.id.signOutButton)

        emailView.text = email

        // TODO: Al mommento de registrarse y si ya existe en la base de datos, buscar otro mecanismo.
//        if(dogWalkerSwitch.isChecked){
//
//            db.collection("dog_walker").document(email).get().addOnSuccessListener {
//                editTextName.setText(it.get("name") as? String)
//                editTextLastName.setText(it.get("last_name") as? String)
//                editTextBirthday.setText(it.get("birthday_year") as? String)
//                editTextPhone.setText(it.get("phone") as? String)
//                editTextCedula.setText(it.get("cedula") as? String)
//            }
//        }else{
//            db.collection("user").document(email).get().addOnSuccessListener {
//                editTextName.setText(it.get("name") as? String)
//                editTextLastName.setText(it.get("last_name") as? String)
//                editTextBirthday.setText(it.get("birthday_year") as? String)
//            }
//        }

        saveButton.setOnClickListener {
            saveButton.isEnabled = false
            signOutButton.isEnabled = false
            if(selectedPhotoUri==null) {
                signOutButton.isEnabled = true
                saveButton.isEnabled = true
                showAlert()}
            else{
                uploadImageToFirebaseStorage(email,saveButton,signOutButton)

            }


        }
        signOutButton.setOnClickListener {
            saveButton.isEnabled = false
            signOutButton.isEnabled = false
            db.collection("dog_walker").document(email).delete()
            FirebaseAuth.getInstance().currentUser.delete()
            FirebaseAuth.getInstance().signOut()
            onBackPressed()
            signOutButton.isEnabled = true
            saveButton.isEnabled = true
        }
    }
    private fun uploadImageToFirebaseStorage(
        email: String,
        saveButton: Button,
        signOutButton: Button
    ) = CoroutineScope(Dispatchers.IO).launch {


        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Successfully uploaded image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d(TAG, "File Location: $it")
                    val s = it.toString()
                    saveUserToFirebaseDatabase(s,email,saveButton,signOutButton)
                }
            }
            .addOnFailureListener {
                Log.d(TAG, "Failed to upload image to storage: ${it.message}")
                showAlert()
            }
    }



    private fun showDog(email: String){
        val dogIntent = Intent(this, DogActivity::class.java).apply{
            putExtra("email", email)
        }
        startActivity(dogIntent)
    }

    private fun goToDriverActivity(model:WalkerInfoModel?) {
        Common.currentUser = model
        startActivity(Intent(this,WalkerHomeActivity::class.java))
        finish()
    }

    private fun saveUserToFirebaseDatabase(
        profileImageUrl: String,
        email: String,
        saveButton: Button,
        signOutButton: Button
    ) = CoroutineScope(Dispatchers.IO).async {
        val model = WalkerInfoModel()
        model.name = editTextName.text.toString()
        model.lastName = editTextLastName.text.toString()
        model.birthday = dateTv.text.toString()
        model.Url_Image = profileImageUrl
        model.email = email




        db.collection("dog_walker").document(email).set(model)

        goToDriverActivity(model)
        signOutButton.isEnabled = true
        saveButton.isEnabled = true
    }
    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Please compleate all fields including profile image")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
    
}

