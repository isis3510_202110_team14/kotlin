package com.example.puphe

import com.example.puphe.Model.WalkerInfoModel
import java.lang.StringBuilder

object Common {
    fun buildWelcomeMessage(): String {
        return StringBuilder("Welcome, ")
            .append(currentUser!!.name)
            .append(" ")
            .append(currentUser!!.lastName)
            .toString()
    }

    val WALKER_LOCATION_REFERENCE: String = "WalkerLocation"
    var currentUser: WalkerInfoModel?=null
    val WALKER_INFO_REFERENCE: String = "WalkerInfo"
}