package com.example.puphe

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.puphe.Model.WalkerInfoModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_edit_activity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.*


class ProfileViewActivity : AppCompatActivity(){
    private val db = FirebaseFirestore.getInstance()
    private val db2 = FirebaseAuth.getInstance()
    lateinit var database: FirebaseDatabase
    lateinit var walkerInfoRef: DatabaseReference
    var selectedPhotoUri: Uri? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_edit_activity)

        update_photo_button.alpha = 0f
        //setup
        val bundle = intent.extras
        val email = bundle?.getString("email")
        config(email?:"")

        update_photo_button.setOnClickListener {
            Log.d(HomeActivity.TAG, "Try to show photo selector")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            // proceed and check what the selected image was....
            Log.d(HomeActivity.TAG, "Photo was selected")

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            update_photo_imageview_register.setImageBitmap(bitmap)

//      val bitmapDrawable = BitmapDrawable(bitmap)
//      selectphoto_button_register.setBackgroundDrawable(bitmapDrawable)
        }
    }
    private fun config(email: String){

        database = FirebaseDatabase.getInstance()
        walkerInfoRef = database.getReference(Common.WALKER_INFO_REFERENCE)

        lateinit var database:FirebaseDatabase
        lateinit var walkerInfoRef:DatabaseReference

        title = "Profile"
        val emailView = findViewById<TextView>(R.id.emailView)
        val editTextName = findViewById<TextView>(R.id.editTextName)
        val editTextLastName = findViewById<TextView>(R.id.editTextLastName)
        val editTextBirthday = findViewById<TextView>(R.id.editTextBirthday)
        val saveChangesButton = findViewById<Button>(R.id.save_changes_button)
        val backButton = findViewById<Button>(R.id.back_button)

        emailView.text = Common.currentUser!!.email
        editTextName.text = Common.currentUser!!.name
        editTextLastName.text = Common.currentUser!!.lastName
        editTextBirthday.text = Common.currentUser!!.birthday
        totalMeters.text = Common.currentUser!!.meters

        Picasso.get().load(Common.currentUser!!.Url_Image).into(update_photo_imageview_register)
        calcuLateMoney()


        back_button.setOnClickListener {
            finish()
        }
        saveChangesButton.setOnClickListener {
            saveChangesButton.isEnabled = false
            backButton.isEnabled = false
            if(selectedPhotoUri==null){
                saveChangesButton.isEnabled = true
                backButton.isEnabled = true
                showAlert()
            }
            else{
                uploadImageToFirebaseStorage(email,saveChangesButton,backButton)
                onBackPressed()
            }

        }
    }

    private fun uploadImageToFirebaseStorage(
        email: String,
        saveChangesButton: Button,
        backButton: Button
    )= CoroutineScope(Dispatchers.IO).launch  {

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                Log.d(HomeActivity.TAG, "Successfully uploaded image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d(HomeActivity.TAG, "File Location: $it")
                    val s = it.toString()
                    saveUserToFirebaseDatabase(s,email,saveChangesButton,backButton)
                }
            }
            .addOnFailureListener {
                Log.d(HomeActivity.TAG, "Failed to upload image to storage: ${it.message}")
                showAlert()
            }
    }

    private fun saveUserToFirebaseDatabase(
        profileImageUrl: String,
        email: String,
        saveChangesButton: Button,
        backButton: Button
    ) = CoroutineScope(Dispatchers.IO).async {
        deleteImage(Common.currentUser!!.Url_Image)
        val model = WalkerInfoModel()
        model.name = Common.currentUser!!.name
        model.lastName = Common.currentUser!!.lastName
        model.birthday = Common.currentUser!!.birthday
        model.Url_Image = profileImageUrl
        model.rating = Common.currentUser!!.rating
        model.email = Common.currentUser!!.email
        model.meters =Common.currentUser!!.meters


        Common.currentUser = model



        db.collection("dog_walker").document(FirebaseAuth.getInstance().currentUser!!.email).set(model)
        saveChangesButton.isEnabled = true
        backButton.isEnabled = true

    }

    private fun deleteImage(fileName : String) = CoroutineScope(Dispatchers.IO).launch {
        try{
            var ref  = FirebaseStorage.getInstance().getReferenceFromUrl(fileName)
            ref.delete()

        }
        catch (e:Exception){

        }
    }


    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Missing Image")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
    private fun calcuLateMoney() {
    // Aqui se hace la optencion de 1 documento
        var totalCOst=0.0
        val docRef = db.collection("dog_walker").document(FirebaseAuth.getInstance().currentUser!!.email)

        db.collection("trip")
            .whereEqualTo("dogWalker", docRef)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("ProfileVIewActivity:", "${document.id} => ${document.data}")
                    val cost = document.get("cost").toString()
                    totalCOst = totalCOst + cost.toDouble()

                }
                walks.text = totalCOst.toString()
            }
            .addOnFailureListener { exception ->
                Log.w("ProfileVIewActivity:", "Error getting documents: ", exception)
            }
        walks.text = totalCOst.toString()

    }
}